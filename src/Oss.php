<?php

namespace Qshsoft\aliyun;

use OSS\OssClient;

class Oss {
    protected $AccessId = '';
    protected $AccessKey = '';
    protected $Bucket = '';
    protected $Endpoint = '';
    protected $RegionId = 'cn-hangzhou';
    protected $SignName = '千途';
    protected $ossClient = null;

    public function __construct($config)
    {
        if (empty($config)) {
            return false;
        }
        $this->AccessId = $config['AccessId'];
        $this->AccessKey = $config['AccessKey'];
        $this->Bucket = $config['Bucket'];
        $this->Endpoint = $config['Endpoint'];
        $this->RegionId = $config['RegionId'];
        $this->SignName = $config['SignName'];
        $this->TemplateCode = $config['TemplateCode'];
        $this->ossClient = new OssClient($this->AccessId, $this->AccessKey, $this->Endpoint);
    }

    public function putObject($object, $content)
    {
        $data = ['success' => false, 'message' => ''];
        try{
            $result = $this->ossClient->putObject($this->Bucket, $object, $content);
            $data['success'] = true;
            $data['url'] = $result['info']['url'];
        } catch(OssException $e) {
            $data['message'] = $e->getMessage();
        }
        return $data;
    }


}