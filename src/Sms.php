<?php
/**
 * 基于阿里云SDK的短信类
 * User: kentChen
 * Date: 2020/2/3
 * Time: 11:05
 */

namespace Qshsoft\aliyun;

use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;
class Sms
{
    protected $AccessId = '';
    protected $AccessKey = '';
    protected $Bucket = '';
    protected $RegionId = 'cn-hangzhou';
    protected $SignName = '千途';
    protected $TemplateCode = [];

    public function __construct($config)
    {
        if (empty($config)) {
            return false;
        }
        $this->AccessId = $config['AccessId'];
        $this->AccessKey = $config['AccessKey'];
        $this->Bucket = $config['Bucket'];
        $this->RegionId = $config['RegionId'];
        $this->SignName = $config['SignName'];
        $this->TemplateCode = $config['TemplateCode'];

        AlibabaCloud::accessKeyClient($this->AccessId, $this->AccessKey)
            ->regionId($this->RegionId) // replace regionId as you need
            ->asGlobalClient();
    }

    /**
     * 短信发送接口
     *
     * @param $PhoneNumbers	     String	是	15900000000
     * @param $TemplateCode     Int	    是   1
     * @param $TemplateParam    String	是	code内容
     * @return array
     */
    public function sendSms($PhoneNumbers, $TemplateCode, $TemplateParam)
    {
        try {
            $result = AlibabaCloud::rpcRequest()
                            ->product('Dysmsapi')
                            ->version('2017-05-25')
                            ->action('SendSms')
                            ->method('POST')
                            ->options([
                                'query' => [
                                    'RegionId' => $this->RegionId,
                                    'PhoneNumbers' => $PhoneNumbers,
                                    'SignName' => $this->SignName,
                                    'TemplateCode' => $this->TemplateCode[$TemplateCode],
                                    'TemplateParam' => json_encode(['code' => $TemplateParam]),
                                ],
                            ])
                            ->request();
            return $result->toArray();
        } catch (ClientException $e) {
            echo $e->getErrorMessage() . PHP_EOL;
        } catch (ServerException $e) {
            echo $e->getErrorMessage() . PHP_EOL;
        }
    }

    /**
     * 短信群发接口
     *
     * @param $PhoneNumbers	     String	是	15900000000
     * @param $TemplateCode     Int	    是   1
     * @param $TemplateParam    String	是	code内容
     * @return array
     */
    public function sendBatchSms($PhoneNumbers, $TemplateCode,$TemplateParam)
    {
        try {
            $SignName = [];
            foreach ((array)$PhoneNumbers as $key=>$mobile){
                // 过滤无效号码
                if(!preg_match('/^1([0-9]{10})/',$mobile)){
                    unset($PhoneNumbers[$key]);
                }else{
                    $SignName[] = $this->SignName;
                }
            }
            $result = AlibabaCloud::rpcRequest()
                            ->product('Dysmsapi')
                            ->version('2017-05-25')
                            ->action('SendBatchSms')
                            ->method('POST')
                            ->options([
                                'query' => [
                                    'RegionId' => $this->RegionId,
                                    'PhoneNumberJson' => json_encode($PhoneNumbers),
                                    'TemplateCode' => $TemplateCode,
                                    'SignNameJson' => json_encode($SignName),
                                    'TemplateParamJson' => json_encode($TemplateParam),
                                ],
                            ])
                            ->request();
            return $result->toArray();
        } catch (ClientException $e) {
            echo $e->getErrorMessage() . PHP_EOL;
        } catch (ServerException $e) {
            echo $e->getErrorMessage() . PHP_EOL;
        }
    }


}